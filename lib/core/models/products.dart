class Products {
  String name;
  String description;
  double price;
  bool isFavorite;
  bool isRecomend;
  String image;
  int count;

  Products({
    this.name,
    this.description,
    this.price,
    this.isFavorite,
    this.isRecomend,
    this.image,
    this.count,
  });
}

List<Products> listProduct = [
  Products(
      count: 0,
      name: 'Hamburguesa sencilla',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: false,
      price: 12.0,
      image: 'assets/burgers/burger1.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Mixta',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: false,
      price: 13.0,
      image: 'assets/burgers/burger2.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Super',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: false,
      price: 14.5,
      image: 'assets/burgers/burger1.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Doble carne',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: false,
      price: 15.0,
      image: 'assets/burgers/burger2.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Tocineta',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: false,
      price: 15.5,
      image: 'assets/burgers/burger1.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Especial',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: false,
      price: 17.0,
      image: 'assets/burgers/burger2.png'),
  Products(
      count: 0,
      name: 'Hamburguesa De la casa',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: false,
      price: 20.5,
      image: 'assets/burgers/burger1.png'),
];

List<Products> listRecomends = [
  Products(
      count: 0,
      name: 'Hamburguesa Perfecta',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: true,
      price: 22.0,
      image: 'assets/burgers/burger3.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Mega Ultimate',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: true,
      price: 23.5,
      image: 'assets/burgers/burger4.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Adorada',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: true,
      price: 24.5,
      image: 'assets/burgers/burger5.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Clasica',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: true,
      price: 25.0,
      image: 'assets/burgers/burger6.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Clasica mega',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: true,
      price: 25.5,
      image: 'assets/burgers/burger3.png'),
  Products(
      count: 0,
      name: 'Hamburguesa Costilla doble',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: true,
      price: 27.0,
      image: 'assets/burgers/burger4.png'),
  Products(
      count: 0,
      name: 'Hamburguesa 3 estaciones',
      description:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque molestie libero eget elit tempus, id elementum erat rutrum. Nam eu enim nisi. Pellentesque et sollicitudin nisi. Morbi aliquet et leo eu luctus. Vestibulum varius mollis fringilla. Pellentesque auctor aliquam ex. Etiam blandit massa dictum nisl consectetur, ac iaculis tortor iaculis',
      isFavorite: false,
      isRecomend: true,
      price: 30.5,
      image: 'assets/burgers/burger5.png'),
];
