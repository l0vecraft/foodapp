import 'package:flutter/material.dart';

class Categories {
  String name;
  String image;
  Color colour;

  Categories({this.name, this.image, this.colour});
}

List<Categories> listCategories = [
  Categories(
      name: 'tacos',
      image: 'assets/categories/tacos.svg',
      colour: Colors.yellow[200]),
  Categories(
      name: 'perros',
      image: 'assets/categories/perros.svg',
      colour: Colors.green[200]),
  Categories(
      name: 'hamburguesas',
      image: 'assets/categories/burger.svg',
      colour: Colors.blue[200]),
  Categories(
      name: 'pizzas',
      image: 'assets/categories/pizza.svg',
      colour: Colors.red[200]),
  Categories(
      name: 'alitas',
      image: 'assets/categories/alitas.svg',
      colour: Colors.orange[200]),
  Categories(
      name: 'combos',
      image: 'assets/categories/combo.svg',
      colour: Colors.purple[200]),
  Categories(
      name: 'sandwiches',
      image: 'assets/categories/sandwiches.svg',
      colour: Colors.pink[200]),
  Categories(
      name: 'especiales',
      image: 'assets/categories/especiales.svg',
      colour: Colors.yellow[200]),
  Categories(
      name: 'chuzos',
      image: 'assets/categories/chuzos.svg',
      colour: Colors.cyan[200]),
];
