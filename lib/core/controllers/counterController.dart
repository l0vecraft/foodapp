import 'package:get/state_manager.dart';

class CounterController extends GetxController {
  RxInt _counter = 0.obs;

  int get counter => _counter.value;
  void add() => _counter.value++;
}
