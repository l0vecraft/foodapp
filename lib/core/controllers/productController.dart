import 'package:app_prueba_getx/core/models/products.dart';
import 'package:get/get.dart';

class ProductController extends GetxController {
  Rx<Products> _product = Products().obs;

  Products get product => _product.value;

  void setProduct(Products newValue) {
    _product.update((value) {
      value.name = newValue.name;
      value.count = newValue.count;
      value.description = newValue.description;
      value.image = newValue.image;
      value.isFavorite = newValue.isFavorite;
      value.isRecomend = newValue.isRecomend;
      value.price = newValue.price;
    });
  }

  @override
  void onInit() {
    print(Get.parameters);
  }
}
