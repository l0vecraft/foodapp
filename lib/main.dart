import 'package:animations/animations.dart';
import 'package:app_prueba_getx/routes.dart';
import 'package:app_prueba_getx/ui/pages/homePage.dart';
import 'package:flutter/material.dart';
import 'package:get/route_manager.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData().copyWith(
          primaryColor: Colors.green,
          pageTransitionsTheme: PageTransitionsTheme(
              builders: <TargetPlatform, PageTransitionsBuilder>{
                TargetPlatform.linux: SharedAxisPageTransitionsBuilder(
                    transitionType: SharedAxisTransitionType.horizontal)
              })),
      home: HomePage(),
      getPages: Routes().rutas,
    );
  }
}
