import 'package:app_prueba_getx/ui/pages/detailPage.dart';
import 'package:get/route_manager.dart';

import 'ui/pages/homePage.dart';
import 'ui/pages/secondPage.dart';
import 'ui/pages/todoPage.dart';

class Routes {
  //! A este tipo de rutas se les puede colocar
  //! Hasta transiciones
  List<GetPage> rutas = [
    GetPage(name: '/', page: () => HomePage()),
    GetPage(name: '/second', page: () => SecondPage()),
    GetPage(
        name: '/detail', page: () => DetailPage(), transition: Transition.zoom),
    GetPage(
        name: '/todo',
        page: () => TodoPage(),
        transition: Transition.topLevel,
        transitionDuration: Duration(milliseconds: 500))
  ];
}
