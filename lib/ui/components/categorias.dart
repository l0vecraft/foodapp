import 'package:app_prueba_getx/core/models/categories.dart';
import 'package:app_prueba_getx/ui/widgets/categoryWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class Categorias extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 30, horizontal: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Explorar categorias',
                style: TextStyle(
                    color: Colors.deepPurple,
                    fontSize: 22,
                    fontWeight: FontWeight.w800),
              ),
              InkWell(
                onTap: () => print('Ver mas'),
                splashColor: Colors.purple[200],
                hoverColor: Colors.transparent,
                customBorder: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)),
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 2),
                  child: Text(
                    'Ver mas',
                    style: TextStyle(fontSize: 14, color: Colors.grey),
                  ),
                ),
              )
            ],
          ),
          Container(
            height: 120,
            child: ListView.builder(
                itemCount: listCategories.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, i) => Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 8),
                      child: CatergoryWidget(category: listCategories[i]),
                    )),
          ),
        ],
      ),
    );
  }
}
