import 'package:app_prueba_getx/core/models/products.dart';
import 'package:app_prueba_getx/ui/widgets/cards/recomendCard.dart';
import 'package:flutter/material.dart';

class Recomendados extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              'Recomendados',
              style: TextStyle(
                  color: Colors.deepPurple,
                  fontSize: 22,
                  fontWeight: FontWeight.w800),
            ),
          ),
          Container(
            height: 200,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 2),
                itemCount: listRecomends.length,
                itemBuilder: (context, i) => Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: RecomendCard(
                        products: listRecomends[i],
                      ),
                    )),
          ),
        ],
      ),
    );
  }
}
