import 'package:app_prueba_getx/core/models/products.dart';
import 'package:app_prueba_getx/ui/widgets/cards/popularCard.dart';
import 'package:flutter/material.dart';

class Populares extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 8,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: Text(
              'Productos Populares',
              style: TextStyle(
                  color: Colors.deepPurple,
                  fontSize: 22,
                  fontWeight: FontWeight.w800),
            ),
          ),
          Container(
            height: 275,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 2),
                itemCount: listProduct.length,
                itemBuilder: (context, i) => Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: PopularCard(
                        products: listProduct[i],
                      ),
                    )),
          ),
        ],
      ),
    );
  }
}
