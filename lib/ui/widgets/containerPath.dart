import 'package:flutter/material.dart';

class ContainerPath extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: size.width,
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
            color: Colors.blue,
          ),
          ClipPath(
            clipper: MyCustomClipPath(),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.amber,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class MyCustomClipPath extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    Path path = Path();
    path.moveTo(size.width, size.height / 2);
    path.cubicTo(size.width / 1.1, size.height / 1.3, size.width / 30,
        size.height / 4, size.width - size.width * 1.2, size.height + 20);
    path.lineTo(size.width, size.height);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper oldClipper) {
    return true;
  }
}
