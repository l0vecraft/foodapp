import 'package:flutter/material.dart';

class MyCustomPaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()..color = Colors.white;
    // ..style = PaintingStyle.stroke
    // ..strokeWidth = 2.0;

    //*esta seria una figura cuadratica
    Path path = Path();
    path.moveTo(size.width, size.height / 2);
    // path.quadraticBezierTo(
    //     size.width / 2, size.height / 8, size.width / 2, size.height / 2);
    // path.close();
    // canvas.drawPath(path, paint);
    //* y esta una cubica que es la que necesito
    //path.moveTo(size.width, 0); // ?lo muevo a la derecha
    path.cubicTo(size.width / 1.02, size.height / 1.3, size.width / 50,
        size.height / 4, size.width / 1.18 - size.width, size.height);
    path.lineTo(size.width, size.height);
    canvas.drawPath(path, paint);
    //* otro dibujo
    // paint.color = Colors.white;
    // var rect = Rect.fromLTRB(0, 0, size.width, size.height);
    // canvas.drawRect(rect, paint);

    //* haremos un custom path
    // paint.color = Colors.yellow;
    // final path = Path();
    // path.lineTo(0, size.height);
    // path.lineTo(size.width, 0);
    // path.close();
    // canvas.drawPath(path, paint);

    //* primer dibujo
    // paint.color = Colors.deepOrange;
    // var center = Offset(size.width / 2, size.height / 2);

    // canvas.drawCircle(center, 120, paint);
    //? Center para este caso es simplemente la ubicacion del criculo
    //? luego el radio definira el tamaño del dibujo
    //? luego el objeto paint
    //--------------------------------------------------
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
