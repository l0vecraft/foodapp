import 'package:app_prueba_getx/core/models/categories.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CatergoryWidget extends StatelessWidget {
  final Categories category;
  CatergoryWidget({@required this.category});
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Container(
          height: 80,
          width: 75,
          padding: EdgeInsets.symmetric(horizontal: 4.5),
          child: SvgPicture.asset(
            category.image,
          ),
          decoration: BoxDecoration(
              color: category.colour,
              boxShadow: [
                BoxShadow(
                  color: category.colour,
                  offset: Offset(0.5, 1),
                )
              ],
              borderRadius: BorderRadius.circular(15)),
        ),
        SizedBox(
          height: 5,
        ),
        Text('${category.name}',
            style: TextStyle(
                color: Colors.deepPurple, fontWeight: FontWeight.bold))
      ],
    ));
  }
}
