import 'package:app_prueba_getx/core/controllers/productController.dart';
import 'package:app_prueba_getx/core/models/products.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PopularCard extends StatelessWidget {
  final Products products;

  PopularCard({this.products});
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductController>(
      init: ProductController(),
      builder: (bloc) => Card(
        elevation: 3,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
        child: Container(
          width: 230,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: IconButton(
                    icon: Icon(
                      products.isFavorite
                          ? Icons.favorite
                          : Icons.favorite_border,
                      color: Colors.red,
                    ),
                    onPressed: null),
              ),
              Center(
                child: Container(
                  height: 120,
                  width: 120,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(color: Colors.cyan[300], blurRadius: 10)
                      ],
                      borderRadius: BorderRadius.circular(60),
                      image: DecorationImage(
                          image: AssetImage(
                            products.image,
                          ),
                          fit: BoxFit.fill)),
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  '${products.name}',
                  style: TextStyle(fontSize: 18),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Text(
                  'Descripcion sencilla del producto',
                  style: TextStyle(color: Colors.grey, fontSize: 12),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('\$${products.price}',
                        style: TextStyle(
                          color: Colors.deepPurple,
                          fontSize: 16,
                          fontWeight: FontWeight.w800,
                        )),
                    Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Colors.deepPurple,
                              blurRadius: 4.0,
                            )
                          ]),
                      child: IconButton(
                          icon: Icon(
                            Icons.arrow_forward_ios,
                            size: 12,
                          ),
                          onPressed: () {
                            bloc.setProduct(products);
                            Get.toNamed('/detail', arguments: products);
                          }),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
