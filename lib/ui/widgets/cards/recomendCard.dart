import 'package:app_prueba_getx/core/controllers/productController.dart';
import 'package:app_prueba_getx/core/models/products.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RecomendCard extends StatelessWidget {
  final Products products;
  RecomendCard({this.products});
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      child: GetBuilder<ProductController>(
        init: ProductController(),
        builder: (bloc) => Container(
          width: 400,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                child: Container(
                  height: 160,
                  width: 160,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      image: DecorationImage(
                          image: AssetImage(products.image),
                          fit: BoxFit.cover)),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Row(
                      children: [
                        Text(
                          'Categoria X',
                          style: TextStyle(color: Colors.grey, fontSize: 15),
                        ),
                        SizedBox(
                          width: 80,
                        ),
                        IconButton(
                            icon: Icon(
                              products.isFavorite
                                  ? Icons.favorite
                                  : Icons.favorite_border,
                              color: Colors.red,
                            ),
                            onPressed: null)
                      ],
                    ),
                  ),
                  Text(products.name,
                      style: TextStyle(
                          fontSize: products.name.length < 20 ? 18 : 15.5,
                          color: Colors.deepPurple)),
                  Text('Descripcion corta',
                      style: TextStyle(color: Colors.grey)),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('\$${products.price}',
                            style: TextStyle(
                              color: Colors.deepPurple,
                              fontSize: 16,
                              fontWeight: FontWeight.w800,
                            )),
                        SizedBox(
                          width: 120,
                        ),
                        Container(
                          height: 30,
                          width: 30,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.deepPurple,
                                  blurRadius: 4.0,
                                )
                              ]),
                          child: IconButton(
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                size: 12,
                              ),
                              onPressed: () {
                                bloc.setProduct(products);
                                Get.toNamed('/detail', arguments: products);
                              }),
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
