import 'package:app_prueba_getx/core/controllers/productController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomAppBarr extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<ProductController>(
      init: ProductController(),
      builder: (bloc) => Container(
        padding: EdgeInsets.symmetric(
          horizontal: 20,
        ),
        height: size.height / 15,
        width: size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
                child: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
                size: 25,
              ),
              onPressed: () => Get.back(),
            )),
            bloc.product.isRecomend
                ? Container()
                : Expanded(
                    child: Center(
                    child: Text(
                      '${bloc.product.name}',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 24,
                          shadows: [
                            BoxShadow(color: Colors.black, blurRadius: 2)
                          ],
                          fontWeight: FontWeight.w900),
                    ),
                  )),
            Container()
          ],
        ),
      ),
    );
  }
}
