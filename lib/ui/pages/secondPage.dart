import 'package:app_prueba_getx/ui/components/categorias.dart';
import 'package:app_prueba_getx/ui/components/populares.dart';
import 'package:app_prueba_getx/ui/components/recomendados.dart';
import 'package:app_prueba_getx/ui/widgets/containerPath.dart';
import 'package:app_prueba_getx/ui/widgets/myCustomPaint.dart';
import 'package:flutter/material.dart';

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 8,
          centerTitle: true,
          leadingWidth: 300,
          leading: Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 8),
            child: TextField(
              decoration: InputDecoration(
                  filled: true,
                  hintText: 'Search',
                  hintStyle: TextStyle(height: .5),
                  focusColor: Colors.purple,
                  prefixIcon: Icon(Icons.search_outlined),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey))),
            ),
          ),
          title: Text(
            'Incio',
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.purple),
          ),
          actions: [
            IconButton(
                icon: Icon(Icons.notifications_outlined, color: Colors.green),
                onPressed: () {}),
            IconButton(
                icon: Icon(Icons.monetization_on_outlined,
                    color: Colors.pinkAccent),
                onPressed: () {}),
          ],
        ),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [Categorias(), Populares(), Recomendados()],
            ),
          ),
        )
        //ContainerPath(),
        // body: Container(
        //     child: CustomPaint(
        //         painter: MyCustomPaint(),
        //         child: Center(
        //           child: Text('Segunda pagina bebe'),
        //         ))),
        );
  }
}
