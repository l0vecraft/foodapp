import 'package:app_prueba_getx/core/controllers/productController.dart';
import 'package:app_prueba_getx/ui/widgets/appbar/customAppBar.dart';
import 'package:app_prueba_getx/ui/widgets/buttons/customButton.dart';
import 'package:app_prueba_getx/ui/widgets/myCustomPaint.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class DetailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GetBuilder<ProductController>(
      init: ProductController(),
      builder: (bloc) => Scaffold(
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Positioned(
              left: 0,
              child: Row(
                children: [
                  Text('${bloc.product.name}',
                      style: TextStyle(color: Colors.red))
                ],
              ),
            ),
            Container(
                color: Colors.blue,
                child: Padding(
                  padding: EdgeInsets.only(bottom: size.height / 4.1),
                  child: Image.asset(
                    bloc.product.image,
                    height: size.height,
                    width: size.width,
                    fit: BoxFit.cover,
                  ),
                )),
            // ClipPath(
            //   clipper: MyCustomClipPath(),
            //   child: Container(
            //     decoration: BoxDecoration(
            //       color: Colors.white,
            //     ),
            //   ),
            // ),
            CustomPaint(
              painter: MyCustomPaint(),
              child: Container(),
            ),
            Positioned(
              right: size.width / 13,
              bottom: size.height / 2.6,
              child: CircleAvatar(
                backgroundColor: Colors.pink,
                maxRadius: 30,
                minRadius: 25,
                child: Center(
                  child: Padding(
                    padding: EdgeInsets.only(right: 8.0, bottom: 4),
                    child: IconButton(
                        icon: Icon(
                          Icons.favorite_border,
                          size: 40,
                          color: Colors.white,
                        ),
                        onPressed: () {}),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 0,
              child: CustomAppBarr(),
            ),
            Positioned(
                bottom: size.height / 22,
                left: size.width / 9,
                child: Container(
                    child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Descripción',
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 23,
                            color: Colors.deepPurple),
                      ),
                      Container(
                        height: size.height / 8,
                        width: size.width / 1.5,
                        padding: EdgeInsets.only(top: size.height / 40),
                        child: Text(
                          '${bloc.product.description}',
                          style: TextStyle(color: Colors.deepPurple),
                        ),
                      ),
                      Row(
                        children: [
                          CustomButtom(
                            text: 'Ordenar ahora',
                          ),
                          SizedBox(
                            width: size.height / 9,
                          ),
                          Text('\$${bloc.product.price}',
                              style: TextStyle(
                                  color: Colors.deepPurple,
                                  fontSize: 28,
                                  fontWeight: FontWeight.w900))
                        ],
                      )
                    ],
                  ),
                ))),
          ],
        ),
      ),
    );
  }
}

class MyCustomClipPath extends CustomClipper<Path> {
  @override
  getClip(Size size) {
    Path path = Path();
    path.moveTo(size.width, size.height / 2);
    path.cubicTo(size.width / 1.1, size.height / 1.3, size.width / 30,
        size.height / 4, size.width - size.width * 1.2, size.height + 20);
    path.lineTo(size.width, size.height);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper oldClipper) {
    return true;
  }
}
