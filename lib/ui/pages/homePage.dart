import 'package:app_prueba_getx/core/controllers/counterController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder(
      init: CounterController(),
      builder: (CounterController bloc) => Scaffold(
        appBar: AppBar(),
        body: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Obx(() => Text('${bloc.counter}')),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: RaisedButton(
                    onPressed: () {
                      /*
                      * Hay varias formas de navegar con Get, enumerare unas cuantas 
                      * con lo que hacen
                      ?1. Get.to()   => y se mueve a la pagina o widget
                      ?2. Get.back() => regresaria
                      ?3. Get.off()  => sacaria la pagina anterior del stack
                      * por ejemplo, estamos en A, nos movemos a B y luego a C
                      * Actualmente estamos en C usando off regresariamos a A sin 
                      * pasar por B
                      ?4. Get.offAll()=> saca todas las paginas y regresa al principio
                      ?5. Get.toName('nombre')=> y se mueve a una ruta nombrada
                       */
                      Get.toNamed('/second');
                    },
                    child: Text('Second Page'),
                  ),
                ),
                RaisedButton(
                  onPressed: () {
                    Get.toNamed('/todo');
                  },
                  child: Text('Todo Page'),
                )
              ],
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => bloc.add(),
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}
